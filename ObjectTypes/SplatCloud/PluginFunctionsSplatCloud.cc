
/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/



//================================================================
//
//  Plugin Functions for SplatClouds
//
//================================================================


//== INCLUDES ====================================================


#include "PluginFunctionsSplatCloud.hh"

#include <OpenFlipper/BasePlugin/PluginFunctions.hh>


//== NAMESPACES ==================================================


namespace PluginFunctions {


//== IMPLEMENTATION ==============================================


//================================================================
// Get objects
//================================================================


bool getObject( int _identifier, SplatCloudObject *&_object )
{
  if( _identifier == BaseObject::NOOBJECT )
  {
    _object = nullptr;
    return false;
  }

  // Get object by using the map accelerated plugin function
  BaseObjectData* object = nullptr;
  PluginFunctions::getObject(_identifier,object);

  _object = dynamic_cast<SplatCloudObject *>( object );
  return (_object != nullptr);
}


//================================================================
// Getting data from objects and casting between them
//================================================================


ShaderNode* splatShaderNode( BaseObjectData *_object )
{
  if( !_object )
    return nullptr;

  if( !_object->dataType( DATA_SPLATCLOUD ) )
    return nullptr;

  SplatCloudObject *object = dynamic_cast<SplatCloudObject *>( _object );

  if ( object == nullptr )
    return nullptr;

  return object->shaderNode();
}


//----------------------------------------------------------------


SplatCloudNode* splatCloudNode( BaseObjectData *_object )
{
  if( !_object )
    return nullptr;

  if( !_object->dataType( DATA_SPLATCLOUD ) )
    return nullptr;

  SplatCloudObject *object = dynamic_cast<SplatCloudObject *>( _object );

  if ( object == nullptr )
    return nullptr;

  return object->splatCloudNode();
}


//----------------------------------------------------------------


SplatCloud* splatCloud( BaseObjectData *_object )
{
  if( !_object )
    return nullptr;

  if( !_object->dataType( DATA_SPLATCLOUD ) )
    return nullptr;

  SplatCloudObject *object = dynamic_cast<SplatCloudObject *>( _object );

  if ( object == nullptr )
    return nullptr;

  return object->splatCloud();
}


//----------------------------------------------------------------


SplatCloud* splatCloud( int _objectId )
{
  auto object = splatCloudObject(_objectId);
  if (object == nullptr)
    return nullptr;
  return object->splatCloud();
}


//----------------------------------------------------------------


SplatCloudObject* splatCloudObject( BaseObjectData *_object )
{
  if( !_object )
    return nullptr;

  if( !_object->dataType( DATA_SPLATCLOUD ) )
    return nullptr;

  return dynamic_cast<SplatCloudObject *>( _object );
}


//----------------------------------------------------------------


SplatCloudObject* splatCloudObject(  int _objectId ) {

  if  (_objectId == BaseObject::NOOBJECT)
    return nullptr;

  // Get object by using the map accelerated plugin function
  BaseObjectData* object = nullptr;
  PluginFunctions::getObject(_objectId,object);

  if ( object == nullptr )
    return nullptr;

  SplatCloudObject* splatCloudObject = dynamic_cast< SplatCloudObject* >(object);

  return splatCloudObject;
}


//================================================================


} // namespace PluginFunctions
